#include <cuda.h>

#include <getopt.h>

#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#include <thrust/device_vector.h>
#include <thrust/equal.h>
#include <thrust/generate.h>
#include <thrust/host_vector.h>
#include <thrust/pair.h>
#include <thrust/system/cuda/execution_policy.h>
#include <thrust/system/cuda/vector.h>

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <map>

using thrust::device_vector;
using thrust::host_vector;

struct Args
{
	size_t height;
	size_t width;

	long steps;
	long converge_check_every;

	float cx;
	float cy;

	Args()
		: height(1000)
		, width(1000)
		, steps(1000)
		, converge_check_every(LONG_MAX)
		, cx(0.1)
		, cy(0.1)
	{
	}
};

Args parse_args(int argc, char** argv)
{
	using std::isfinite;

	Args args;
	bool valid = true;

	int ch;
	while ((ch = getopt(argc, argv, "w:h:s:c:x:y:")) != -1) {
		switch (ch) {
		case 'w':
			errno = 0;
			args.width = strtol(optarg, NULL, 10);
			// Need to make sure addition of 2 in width won't overflow
			if (args.width <= 0 || (size_t)(-1) - 2 < uintmax_t(args.width) || errno != 0) {
				valid = false;
			}
			break;

		case 'h':
			errno = 0;
			args.height = strtol(optarg, NULL, 10);
			// Need to make sure addition of 2 in height won't overflow
			if (args.height <= 0 || (size_t)(-1) - 2 < uintmax_t(args.height) || errno != 0) {
				valid = false;
			}
			break;

		case 's':
			errno = 0;
			args.steps = strtol(optarg, NULL, 10);
			if (args.steps <= 0 || errno != 0) {
				valid = false;
			}
			break;

		case 'c':
			errno = 0;
			args.converge_check_every = strtol(optarg, NULL, 10);
			if (args.converge_check_every <= 0 || errno != 0) {
				valid = false;
			}
			break;

		case 'x':
			args.cx = strtod(optarg, NULL);
			if (!isfinite(args.cx) || args.cx <= 0 || 1 <= args.cx || errno != 0) {
				valid = false;
			}
			break;

		case 'y':
			args.cy = strtod(optarg, NULL);
			if (!isfinite(args.cy) || args.cy <= 0 || 1 <= args.cy || errno != 0) {
				valid = false;
			}
			break;

		default:
			valid = false;
			break;
		}
	}

	if (!valid) {
		fprintf(stderr, "Invalid arguments passed\n");

		exit(EXIT_FAILURE);
	}

	if (((size_t)(-1) / sizeof(float)) / (args.height + 2) < (args.width + 2)) {
		fprintf(stderr, "Grid size would cause an integer overflow\n");

		exit(EXIT_FAILURE);
	}

	return args;
}

void print_grid(const host_vector<float>& grid, size_t height, size_t width, const char* outfile)
{
	FILE* out = fopen(outfile, "w");
	if (out == NULL) {
		perror("File creation failed");
		return;
	}

	for (size_t i = 0; i < height; i++) {
		for (size_t j = 0; j < width; j++) {
			fprintf(out, "%6.1f%c", grid[i * width + j], j == width - 1 ? '\n' : ' ');
		}
	}

	fclose(out);
}

class cached_allocator
{
	public:
		typedef char value_type;

		cached_allocator()
		{
		}

		~cached_allocator()
		{
			for (free_blocks_type::iterator i = free_blocks.begin();
					i != free_blocks.end(); ++i)
			{
				thrust::cuda::free(thrust::cuda::pointer<char>(i->second));
			}

			for (allocated_blocks_type::iterator i = allocated_blocks.begin();
					i != allocated_blocks.end(); ++i)
			{
				thrust::cuda::free(thrust::cuda::pointer<char>(i->first));
			}
		}

		char* allocate(std::ptrdiff_t num_bytes)
		{
			char* result = 0;

			free_blocks_type::iterator free_block = free_blocks.find(num_bytes);
			if (free_block != free_blocks.end()) {
				result = free_block->second;
				free_blocks.erase(free_block);
			} else {
				try {
					result = thrust::cuda::malloc<char>(num_bytes).get();
				} catch(std::runtime_error &e) {
					throw;
				}
			}

			allocated_blocks.insert(std::make_pair(result, num_bytes));
			return result;
		}

		void deallocate(char* ptr, size_t n)
		{
			allocated_blocks_type::iterator iter = allocated_blocks.find(ptr);
			std::ptrdiff_t num_bytes = iter->second;
			allocated_blocks.erase(iter);

			free_blocks.insert(std::make_pair(num_bytes, ptr));
		}

	private:
		typedef std::multimap<std::ptrdiff_t, char*> free_blocks_type;
		typedef std::map<char *, std::ptrdiff_t> allocated_blocks_type;

		free_blocks_type free_blocks;
		allocated_blocks_type allocated_blocks;
};

struct ApproxEquals
{
	__host__ __device__
	bool operator()(float lhs, float rhs) const
	{
		return fabsf(lhs - rhs) < FLT_EPSILON;
	}
};

__global__ void step(float* dest, const float* src,
		int width, int height, float cx, float cy)
{
	int col = blockIdx.x * blockDim.x + threadIdx.x + 1;
	int row = blockIdx.y * blockDim.y + threadIdx.y + 1;
	if (col >= width - 1 | row >= height - 1) {
		return;
	}

	int idx = row * width + col;

	int top = idx - width;
	int bot = idx + width;

	int left = idx - 1;
	int right = idx + 1;

	float initial = src[idx];

	dest[idx] = initial
		+ cy * (src[top] + src[bot] - 2 * initial)
		+ cx * (src[left] + src[right] - 2 * initial);
}

int main(int argc, char** argv)
{
	Args args = parse_args(argc, argv);

	float cx = args.cx;
	float cy = args.cy;

	int width = args.width + 2;
	int height = args.height + 2;

	host_vector<float> init(width * height);

	printf("Creating input grid...\n");

	for (int i = 1; i < height - 1; i++) {
		for (int j = 1; j < width - 1; j++) {
			init[i * width + j] = i * (width - i - 1) * j * (height - j - 1);
		}
	}

	printf("Writing input grid to file...\n");
	print_grid(init, height, width, "infile.dat");

	dim3 block(192, 1);

	size_t blocks_w = width / block.x + (width  % block.x != 0);
	size_t blocks_h = height / block.y + (height % block.y != 0);
	dim3 grid(blocks_w, blocks_h);

	device_vector<float> in(init);
	device_vector<float> out(in.size(), 0);

	cached_allocator alloc;

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);

	for (long i = 0; i < args.steps; i++) {
		step<<<grid, block>>>(thrust::raw_pointer_cast(out.data()),
				thrust::raw_pointer_cast(in.data()), width, height, cx, cy);

		if (i != 0 && i % args.converge_check_every == 0) {
			bool converged = thrust::equal(thrust::cuda::par(alloc),
					in.begin(), in.end(), out.begin(), ApproxEquals());

			if (converged) {
				printf("Convergence achieved after: %ld\n", i);
				break;
			}
		}

		in.swap(out);
	}

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	float milliseconds = 0;
	cudaEventElapsedTime(&milliseconds, start, stop);
	printf("Elapsed time: %.3f seconds\n", milliseconds / 1000);

	thrust::copy(in.begin(), in.end(), init.begin());

	printf("Writing output grid to file...\n");
	print_grid(init, height, width, "outfile.dat");

	return EXIT_SUCCESS;
}
